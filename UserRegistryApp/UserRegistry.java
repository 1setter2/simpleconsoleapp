package UserRegistryApp;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserRegistry {

    private static final String USER_REGISTRY_FILE_NAME = "/user-registry";

    private List<User> users;

    public UserRegistry()
            throws IOException {
        initializeFileIfNotExists();
        readUsersFromFile();
    }

    public void add(String name, Integer age)
            throws IOException {
        User u = new User(name, age);
        users.add(u);
        writeUsersToFile();
        System.out.print("Added " + name + " [" + age + "yo]");
        System.out.print(System.lineSeparator());
    }

    public void delete(String name)
            throws IOException {
        User user = null;
        for (User u : users) {
            if (u.getName().equals(name)) {
                user = u;
                break;
            }
        }
        if (user != null) {
            users.remove(user);
            writeUsersToFile();
            System.out.print("Deleted " + name);
            System.out.print(System.lineSeparator());
        } else {
            System.out.print("User " + name + " does not exist");
            System.out.print(System.lineSeparator());
        }
    }

    public void list() {
        int i = 1;
        for (User u : users) {
            System.out.print(i + ". " + u.getName() + " [" + u.getAge() + "yo]");
            System.out.print(System.lineSeparator());
            i++;
        }
    }

    private void readUsersFromFile()
            throws IOException {
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        try {
            fin = new FileInputStream(USER_REGISTRY_FILE_NAME);
            ois = new ObjectInputStream(fin);
            users = (ArrayList<User>) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                ois.close();
            }
            if (fin != null) {
                fin.close();
            }
        }
    }

    private void writeUsersToFile()
            throws IOException {
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try {
            fout = new FileOutputStream(USER_REGISTRY_FILE_NAME);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(users);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (fout != null) {
                fout.close();
            }
        }
    }

    private void initializeFileIfNotExists()
            throws IOException {
        File registry = new File(USER_REGISTRY_FILE_NAME);
        boolean created = registry.createNewFile();
        if (created) {
            users = new ArrayList<>();
            writeUsersToFile();
        }
    }

}

