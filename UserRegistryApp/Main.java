package UserRegistryApp;

public class Main {

    public static void main(String[] args) {
        try {
            UserRegistry r = new UserRegistry();
            Command c = resolveCommand(args);
            switch (c) {
                case ADD:
                    r.add(getName(args), getAge(args));
                    break;
                case DELETE:
                    r.delete(getName(args));
                    break;
                case LIST:
                    r.list();
                    break;
            }
        } catch (Exception e) {
            System.out.print("Application failed due to an exception:");
            System.out.print(System.lineSeparator());
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static Command resolveCommand(String[] args) {
        if (args.length > 0) {
            String c = args[0];
            if ("ADD".equals(c)) {
                return Command.ADD;
            } else if ("DELETE".equals(c)) {
                return Command.DELETE;
            } else if ("LIST".equals(c)) {
                return Command.LIST;
            }
        }
        throw new IllegalArgumentException("ADD, DELETE or LIST command expected");
    }

    private static String getName(String[] args) {
        if (args.length > 1) {
            return args[1];
        }
        throw new IllegalArgumentException("Name argument expected");
    }

    private static Integer getAge(String[] args) {
        if (args.length > 2) {
            return Integer.parseInt(args[2]);
        }
        throw new IllegalArgumentException("Age argument expected");
    }

}
